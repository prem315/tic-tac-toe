import React from "react";
import Board from "./components/Board";
import "./App.css";
import cross from "./images/crossShadow1.png";
import circle from "./images/circleShadow1.png";
import Popup from "./components/Popup";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      gamePage: false,
      player: null,
      selectedPlayer: null,
    };
  }
  setPlayer(event) {
    console.log(event.target.value);
    this.setState({
      player: event.target.value,
    });
  }

  submitPlayer = (e) => {
    e.preventDefault();
    if (this.state.player === "X") {
      this.setState({
        selectedPlayer: true,
        gamePage: true,
      });
    } else {
      this.setState({
        selectedPlayer: false,
        gamePage: true,
      });
    }
  };

  toggleGamePage() {
    this.setState({
      gamePage: !this.state.gamePage,
    });
  }
  render() {
    if (this.state.gamePage === false) {
      return (
        <div className="App">
          <div className="app-box">
            <p className="header-text">Pick your side</p>

            <div
              className="player-container"
              onChange={this.setPlayer.bind(this)}
            >
              <label className="pl-cotainer">
                <img className="img" src={cross} />
                <input
                  className="radio1"
                  name="player"
                  value="X"
                  type="radio"
                />
                <span className="checkmark"></span>
              </label>
              <label className="pl-cotainer">
                <img className="img" src={circle} />
                <input
                  className="radio2"
                  type="radio"
                  name="player"
                  value="O"
                  type="radio"
                />
                <span className="checkmark"></span>
              </label>
            </div>
            <div className="btn-box">
              <button className="player-btn" onClick={this.submitPlayer}>
                Continue
              </button>
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <div className="App">
          <div className="app-box">
            <Board
              xIsNext={this.state.selectedPlayer}
              changePage={this.toggleGamePage.bind(this)}
            />
          </div>
        </div>
      );
    }
  }
}

export default App;
