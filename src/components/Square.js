import React from "react";
import cross from "../images/crossShadow1.png";
import circle from "../images/circleShadow1.png";

class Square extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: null,
    };
  }

  render() {
    return (
      <button className="" onClick={() => this.props.onClick()}>
        {this.props.value === "X" ? (
          <img src={cross} style={{ width: "85px" }} />
        ) : null}
        {this.props.value === "O" ? (
          <img src={circle} style={{ width: "85px" }} />
        ) : null}
      </button>
    );
  }
}

export default Square;
