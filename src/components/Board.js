import React from "react";
import Square from "./Square";
import settings from "../images/settings.svg";
import Popup from "./Popup";

// calculate winner 

const calculateWinner = (squares) => {
  console.log(squares);
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    console.log(a, b, c);
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      console.log(squares[a], squares[b], squares[c]);
      return squares[a];
    }
  }
  return null;
};

class Board extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      squares: Array(9).fill(null),
      xIsNext: this.props.xIsNext,
      countX: 0,
      countO: 0,
      winner: null,
      showPopup: true,
    };
  }

  handleClick(i) {
    const squares = this.state.squares.slice();
    squares[i] = this.state.xIsNext ? "X" : "O";
    this.setState({
      squares: squares,
      xIsNext: !this.state.xIsNext,
    });
  }

  renderSquare(i) {
    return (
      <Square
        value={this.state.squares[i]}
        onClick={() => this.handleClick(i)}
      />
    );
  }

  togglePopup() {
    this.setState({
      showPopup: !this.state.showPopup,
    });
    this.props.changePage();
  }

  render() {
    const winner = calculateWinner(this.state.squares);

    let status;
    if (winner) {
      status = "Winner: " + winner;
    } else {
      status = "Next player: " + (this.state.xIsNext ? "X" : "O");
    }

    return (
      <>
        <div className="status">{status}</div>
        {this.state.showPopup && winner === "X" ? (
          <Popup winner="X" closePopup={this.togglePopup.bind(this)} />
        ) : null}
        {this.state.showPopup && winner === "O" ? (
          <Popup winner="O" closePopup={this.togglePopup.bind(this)} />
        ) : null}
        <div className="container-status">
          <div className="status-box-1">
            <p className="score">Player 1</p>
          </div>
          <div className="status-box-2">
            <p className="score">
              {this.state.countX} - {this.state.countX}
            </p>
          </div>
          <div className="status-box-3">
            <p className="score">Player 2</p>
          </div>
        </div>
        <div className="container">
          <div className="box-1">{this.renderSquare(0)}</div>
          <div className="box-2">{this.renderSquare(1)}</div>
          <div className="box-3">{this.renderSquare(2)}</div>
          <div className="box-4">{this.renderSquare(3)}</div>
          <div className="box-5">{this.renderSquare(4)}</div>
          <div className="box-6">{this.renderSquare(5)}</div>
          <div className="box-7">{this.renderSquare(6)}</div>
          <div className="box-8">{this.renderSquare(7)}</div>
          <div className="box-9">{this.renderSquare(8)}</div>
        </div>
        <div className="settings">
          <img className="settings-img" src={settings} />
        </div>
      </>
    );
  }
}

export default Board;
