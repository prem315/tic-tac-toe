import React from "react";
import cross from "../images/crossShadow1.png";
import circle from "../images/circleShadow1.png";

class Popup extends React.Component {
  render() {
    return (
      <div className="popup">
        <div className="popup_inner">
          <div className="popup-container">
            {this.props.winner === "X" ? (
              <>
                <span>Winner is </span>
                <img src={cross} className="cross-winner-img" />
              </>
            ) : null}
            {this.props.winner === "O" ? (
              <>
                <span>Winner is </span>
                <img src={circle} className="circle-winner-img" />
              </>
            ) : null}
          </div>
          <div className="close-btn-box">
            <button className="close-btn" onClick={this.props.closePopup}>
              Play again
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default Popup;
